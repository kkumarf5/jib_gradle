package com.soprabanking.dxp.cicd;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

    @RestController
    public class HelloController {

        private static final String template = "Hello %s! %s-%s";
        private final AtomicLong counter = new AtomicLong();
        private static InetAddress ip;
        @Autowired
        private ApplicationContext context;

        @RequestMapping("/cicd-hello-service")
        public HelloMain hello(@RequestParam(value="name", defaultValue="World") String name) throws UnknownHostException {
            ip = InetAddress.getLocalHost();
            Environment env = (Environment) context.getBean("environment");
            BuildProperties buildProperties = context.getBean(BuildProperties.class);
            return new HelloMain(counter.incrementAndGet(),
                    String.format(template, name, ip.getHostAddress(), buildProperties.getVersion() ));
        }
    }