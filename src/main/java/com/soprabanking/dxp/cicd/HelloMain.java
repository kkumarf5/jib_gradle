package com.soprabanking.dxp.cicd;

public class HelloMain {

        private final long id;
        private final String content;

        public HelloMain(long id, String content) {
            this.id = id;
            this.content = content;
        }

        public long getId() {
            return id;
        }

        public String getContent() {
            return content;
        }
    }